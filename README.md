# Rofi WireGuard Selector

This is a simple Bash script that uses Rofi to display a menu that allows you to choose from a list of WireGuard VPN interfaces.

## Dependancies

* rofi (`dnf install rofi` // `apt install rofi`)
* [adi1090x's rofi themes](https://github.com/adi1090x/rofi)
* WireGuard (`dnf install wireguard` // `apt install wireguard`)

## Menu

The menu allows you to select from the list of WireGuard interfaces.

![](menu.png)

### Setup

1. Edit `wireguard_selector.sh`
2. Enter your list of servers. It should be in the format `servers[interface-name]="Friendly Name"`.
3. Enter the path to your desired Rofi theme file
4. `chmod +x wireguard_selector.sh` to give the script execute permission
5. `./wireguard_selector.sh menu` to run the script

## Polybar Integration

If desired, you can integrate this script with Polybar to add a clickable WireGuard indicator to your bar. The indicator will hide if none of the WireGuard interfaces are up.

### Setup

1. Set up the menu using the above steps
2. Edit `wireguard_selector.sh`
3. Set `polybar_icon_string` to your desired icon, with an optional color around it
4. Edit `~/.config/polybar/config.ini`
5. Create a new module:
    ```
    [module/wireguard]
    type = custom/script
    exec = '/path/to/wireguard_selector.sh' polybar_icon
    interval = 2
    label = "%output%"
    click-left = '/path/to/wireguard_selector.sh' menu
    ```
6. Add the module to one or more of your bars and restart Polybar
